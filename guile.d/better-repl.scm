(use-modules (ice-9 readline))     ; for readline support
(use-modules (texinfo reflection)) ; for more documentation

(activate-readline)
