;;; Floating-point approximation
(define approx=
  (case-lambda
    "Approximative equality operator."
    ((eps)      #t)
    ((eps _)    #t)
    ((eps a b . rest)
     (and (< (abs (- b a)) eps)
          (apply approx= eps a rest)))))
